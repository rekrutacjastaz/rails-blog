class AddTextTypeToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :text_type, :string
  end
end
