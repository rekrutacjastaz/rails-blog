# Rails Blog

### Deployment instructions:

```sh
$ bundle install --without production
$ rake db:migrate
$ rake db:seed
$ rails s   #(port :3000)
```

### Production instructions for Heroku:

```sh
$ heroku login
$ heroku create
$ git push heroku master
$ heroku run rake db:migrate
$ heroku run rake db:seed
$ heroku open
```

### Branches
  - master - stable code deployed on [heroku]
  - develop - application development
  - topics branches - features, issues, bug-fixes, etc.

### TODO
  - [x] admin role
  - [x] author role
  - [x] only author can write articles
  - [x] auto set article author based on login
  - [x] adding comments under articles
  - [x] only registered users can write comments
  - [x] article fields validation
  - [x] comment fields validation
  - [x] database seeds for articles, comments and users
  - [x] searching articles by author
  - [x] pagination
  - [x] admin can delete comments
  - [x] bootstrap

  - [x] markdown/asciidoc articles and comments

[heroku]:https://my-blog-on-rails.herokuapp.com
